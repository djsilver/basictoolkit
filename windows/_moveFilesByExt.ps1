
function moveFiles($src, $searchFilter, $dest) {
	#Check that the dest folder exists, if not, create it
	if(!(test-path $dest)) {
		New-Item -ItemType Directory -Force -Path $dest
	}
	
	$num=1
	Get-ChildItem -Path $src -Filter $searchFilter | ForEach-Object {

		$nextName = Join-Path -Path $dest -ChildPath $_.name

		while(Test-Path -Path $nextName)
		{
		   $nextName = Join-Path $dest ($_.BaseName + "_($num)" + $_.Extension)    
		   $num+=1   
		}

		$_ | Move-Item -Destination $nextName
	}
}


#$src = "."
#$dest = "_gif"

moveFiles "." "*.jpg" "_jpg"
moveFiles "." "*.jpeg" "_jpg"
moveFiles "." "*.png" "_png"
moveFiles "." "*.tiff" "_tiff"
moveFiles "." "*.gif" "_gif"

moveFiles "." "*.mp3" "_mp3"

moveFiles "." "*.mp4" "_mp4"
moveFiles "." "*.flv" "_flv"
moveFiles "." "*.webm" "_webm"

moveFiles "." "*.pdf" "_pdf"

moveFiles "." "*.exe" "_exe"
moveFiles "." "*.msi" "_msi"

moveFiles "." "*.html" "_html"
moveFiles "." "*.htm" "_html"
moveFiles "." "*_files" "_html"

moveFiles "." "*.zip" "_zip"
moveFiles "." "*.7z" "_zip"
moveFiles "." "*.rar" "_zip"

moveFiles "." "*.gz" "_linux"
moveFiles "." "*.tgz" "_linux"
moveFiles "." "*.tar" "_linux"
moveFiles "." "*.sh" "_linux"

moveFiles "." "*.txt" "_txt"
moveFiles "." "*.iso" "_iso"
moveFiles "." "*.epub" "_book"
moveFiles "." "*.mobi" "_book"
moveFiles "." "*.torrent" "_torrent"

moveFiles "." "*.whl" "_mislan"
moveFiles "." "*.xpi" "_mislan"
moveFiles "." "*.QFX" "_mislan"
moveFiles "." "*.xpi" "_mislan"


#Cleanup tmp download files
Get-ChildItem -Path . -Filter *.tmp | foreach ($_) {remove-item $_.fullname}
Get-ChildItem -Path . -Filter *.crdownload | foreach ($_) {remove-item $_.fullname}
