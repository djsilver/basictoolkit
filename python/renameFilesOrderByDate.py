#!/usr/bin/env python
from stat import S_ISREG, ST_MTIME, ST_MODE
import os, sys, time

# path to the directory (relative or absolute)
dirpath = sys.argv[1] if len(sys.argv) == 2 else r'.'

# get all entries in the directory w/ stats
entries = (os.path.join(dirpath, fn) for fn in os.listdir(dirpath))
entries = ((os.stat(path), path) for path in entries)

# leave only regular files, insert creation date
entries = ((stat[ST_MTIME], path)
           for stat, path in entries if S_ISREG(stat[ST_MODE]))
#NOTE: on Windows `ST_CTIME` is a creation date 
#  but on Unix it could be something else
#NOTE: use `ST_MTIME` to sort by a modification date

count = 3463
currentDirName = os.getcwd()
restoreMapping = currentDirName.split('\\')[-1] + "_restoreRenameMapping.txt"

print "Opening file: " + restoreMapping
f = open(restoreMapping,'w')

for cdate, path in sorted(entries):
	#print time.ctime(cdate), os.path.basename(path)
	#print os.path.basename(path)
	
	newFileName = 'DJ14' + '{0:04}'.format(count) + '_rn.' + os.path.basename(path).split('.')[1]
	print "Renaming: " + os.path.basename(path) + " to " + newFileName
	os.rename(os.path.basename(path), newFileName)
	
	
	f.write(os.path.basename(path) + ":" + newFileName + '\n')
	count = count + 1
f.close()