#moveFilesByDay v1.1
### Change log ###
# 1.0 - Works
# 1.1 - Uses local time zone

#!/usr/bin/python2
import os, time, shutil, sys
 

try:
	dir = sys.argv[1]
except:
	dir = "."
os.chdir(dir)
for f in os.listdir('.'):
	if not os.path.isdir(f): #Don't move folders
		if not "moveFilesBy" in f: #Don't move the script file into a folder
			ftime = time.localtime(os.path.getmtime(f))
			#time.localtime(os.path.getmtime(fullFilePath))) #Local time zone
			ctime = time.gmtime(os.path.getctime(f))
			#print(ftime)
			#print(ctime)
			ctime_dir = str(ftime.tm_year)# + '_' + str(ftime.tm_mon)# + '_' + str(ftime.tm_mday)
			
			
			#ctime_dir = ctime_dir + '_' + str(int(ftime.tm_mday))	
				
			if not os.path.isdir(ctime_dir):
				os.mkdir(ctime_dir)
			dst = ctime_dir + '/' + f
			shutil.move(f, dst);
			print('\"' + f + '\" moved to \"' + dst + '\"')
			#shutil.copy(f, dst);
			#shutil.copystat(f, dst);
			#print('\"' + f + '\" copied to \"' + dst + '\"')