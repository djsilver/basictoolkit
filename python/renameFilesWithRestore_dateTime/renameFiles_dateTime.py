import os
import glob
import time

os.chdir(".")
currentPath = os.path.abspath(".")

os.chdir(currentPath)
fileList = []
count = 0
currentDirName = os.getcwd()
restoreMapping = currentDirName.split('\\')[-1] + "_restoreRenameMapping.txt"



# - Get last-modified-date-time
# - Rename the files to be "YYYY-mm-dd_HH-MM-SS_<origFilename>"
# - Move into current folder
def createNewFileName(filenameIn, fullFilePathIn):
	#global oldFileToNewFilePathDict
		print filenameIn
		
		#Check if already renamed
		if "_#" in filenameIn: #Already renamed
			return(filenameIn)
		else: #Note renamed yet
			#print(filenameIn)
			fileDateTime = time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(os.path.getmtime(fullFilePathIn))) #Local time zone
			#print(fileDateTime)
			newFileName = fileDateTime + "_#" + filenameIn
			return(newFileName)

def fileTraversal(currentPath, fileExt):
	
	#
	print "Opening file: " + restoreMapping
	f = open(restoreMapping,'w')
	
	#$# Get list of all *.fileExt files
	fileList = glob.glob("*." + fileExt)
	print("fileList: " + str(fileList))
	
	for filename in fileList:
		#recursiveFileList.append(os.path.join(root, filename))
		#print("root: " + str(root))
		print("filename: " + str(filename))
		
		fullFilePath = os.path.abspath(filename)
		newFileName = createNewFileName(filenameIn=filename, fullFilePathIn=fullFilePath)
		#newFullFilePath = os.path.join(targetFolderPath, newFileName)
		
		print("Renaming: " + filename + " to " + newFileName)
		f.write(filename + ":" + newFileName + '\n') #Used for the restoreMapping
		os.rename(filename, newFileName)
			
			
	f.close()
		
			
	#print(recursiveFileList)



#<<< Main script start >>>#
#combineFolderPath = os.path.abspath(os.path.join(os.path.abspath(os.pardir), "_combine"))

#jpg
fileTraversal(currentPath, fileExt="jpg")

#png
fileTraversal(currentPath, fileExt="png")





