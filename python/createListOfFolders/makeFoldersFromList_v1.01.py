#makeFoldersFromList v1.01
### Change log ###
# 1.0 - Works
# 1.01 - Handles folders already existing that it is trying to create

import os
import shutil
import glob
import traceback

os.chdir(".")
currentPath = os.path.abspath(".")

#Find all text files with lists of files
textFilesList = glob.glob("*_folderList.txt")

for textFile in textFilesList:
	# Create each folder
	# Expecting 1 folder per row in textFile
	with open(textFile) as fh:
		for row in fh:
			folder = row.strip().split(".")[0]
			
			#Check if folder already exists
			if not os.path.isdir(folder):
				print ("Creating folder: " + row)
				try:
					os.makedirs(folder)
				except:
					traceback.print_exc()
			else:
				print ("Already exists: " + row)