import os
import glob

os.chdir(".")
currentPath = os.path.abspath(".")

listOfFolders = next(os.walk('.'))[1] #[0] = parentDirName, [1] = folders, [2] = files

currentDirName = os.getcwd()
fileOutPath = currentDirName.split('\\')[-1] + "_folderList.txt"

print ("folderList " + str(listOfFolders))

print ("Opening file: " + fileOutPath)
f = open(fileOutPath,'w')
for folder in listOfFolders:
	print ("writing: " + folder)
	f.write(folder + '\n')
f.close()
