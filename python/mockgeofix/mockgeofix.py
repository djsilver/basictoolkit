import getpass
import sys
import telnetlib

from random import randint

import threading #Random GPS keep-alive

HOST = "192.168.1.2"
PORT = "5554"

tn = telnetlib.Telnet(HOST,PORT)

#tn.write("ls\n")
#tn.write("exit\n")

#print tn.read_all()

x = -77.478124
y = 38.728273
lastGeo_filePath = "lastGeo.txt"

#################
#Random GPS keep-alive
enableKeepAlive = True
#keepAlive_operandFlip = True
def gps_keepAlive(): 
	global enableKeepAlive
	if enableKeepAlive:
		threading.Timer(3, gps_keepAlive).start ()
		print("GPS keep-alive")

		global x
		global y
		#global keepAlive_operandFlip
		moveChoice = randint(1,4)
		
		#if keepAlive_operandFlip:
			#keepAlive_operandFlip = False
			#x = x + 0.000200 + (float(randint(0,50)) / 10000000)
			#y = y + 0.000200 + (float(randint(0,50)) / 10000000)
		#else:
			#keepAlive_operandFlip = True
			#x = x - 0.000200 - (float(randint(0,50)) / 10000000)
			#y = y - 0.000200 - (float(randint(0,50)) / 10000000)
			
		
		if moveChoice == 1:
			x = x + 0.000100 + (float(randint(0,50)) / 10000000)
			y = y + 0.000100 + (float(randint(0,50)) / 10000000)
		if moveChoice == 2:
			x = x + 0.000100 + (float(randint(0,50)) / 10000000)
			y = y - 0.000100 - (float(randint(0,50)) / 10000000)
		if moveChoice == 3:
			x = x - 0.000100 - (float(randint(0,50)) / 10000000)
			y = y + 0.000100 + (float(randint(0,50)) / 10000000)
		if moveChoice == 4:
			x = x - 0.000100 - (float(randint(0,50)) / 10000000)
			y = y - 0.000100 - (float(randint(0,50)) / 10000000)
		
		
		
		y = round(y,6)
		x = round(x,6)
		commandString = "geo fix " + str(x) + " " + str(y)
		#print("commandString: " + str(commandString))
		handleTelnetSending(commandString + "\n")

#gps_keepAlive()

enableKeepAliveMinimal = False
def gps_keepAliveMinimal(): 
	global enableKeepAliveMinimal
	if enableKeepAliveMinimal:
		threading.Timer(3, gps_keepAliveMinimal).start ()
		print("GPS keep-alive-minimal")

		global x
		global y
		#global keepAlive_operandFlip
		moveChoice = randint(1,4)
		
		#if keepAlive_operandFlip:
			#keepAlive_operandFlip = False
			#x = x + 0.000200 + (float(randint(0,50)) / 10000000)
			#y = y + 0.000200 + (float(randint(0,50)) / 10000000)
		#else:
			#keepAlive_operandFlip = True
			#x = x - 0.000200 - (float(randint(0,50)) / 10000000)
			#y = y - 0.000200 - (float(randint(0,50)) / 10000000)
			
		
		if moveChoice == 1:
			x = x + 0.000000 + (float(randint(0,2)) / 10000000)
			y = y + 0.000000 + (float(randint(0,2)) / 10000000)
		if moveChoice == 2:
			x = x + 0.000000 + (float(randint(0,2)) / 10000000)
			y = y - 0.000000 - (float(randint(0,2)) / 10000000)
		if moveChoice == 3:
			x = x - 0.000000 - (float(randint(0,2)) / 10000000)
			y = y + 0.000000 + (float(randint(0,2)) / 10000000)
		if moveChoice == 4:
			x = x - 0.000000 - (float(randint(0,2)) / 10000000)
			y = y - 0.000000 - (float(randint(0,2)) / 10000000)
		
		
		
		y = round(y,6)
		x = round(x,6)
		commandString = "geo fix " + str(x) + " " + str(y)
		#print("commandString: " + str(commandString))
		handleTelnetSending(commandString + "\n")


#################
#Egg walker
enableEggWalker = False
eggWalker_operandFlip_X = True
eggWalker_operandFlip_Y = True
orig_x = x
orig_y = y
range_x = 0.002
range_y = 0.002
def eggWalker(): 
	global enableEggWalker
	if enableEggWalker:
		threading.Timer(0.5, eggWalker).start ()
		print("eggWalker")

		global x
		global y
		global eggWalker_operandFlip_X
		global eggWalker_operandFlip_Y
		global orig_x
		global orig_y
		global range_x
		global range_y
		
		print("X-diff: " + str(abs(x) - abs(orig_x)))
		print("Y-diff: " + str(abs(y) - abs(orig_y)))
		
		#X pathing
		if (abs(x) - abs(orig_x)) < range_x and eggWalker_operandFlip_X: #Smaller than max and direction hasn't just been reversed
			#y = y + (float(randint(0,90)) / 1000000)
			x = x - 0.000000 - (float(randint(0,25)) / 1000000)
		else:
			print("X-reverse")
			eggWalker_operandFlip_X = False
			#y = y - (float(randint(0,90)) / 1000000)
			x = x + 0.000000 + (float(randint(0,25)) / 1000000)
			
			#Check if need to reverse direction back to original direction
			if abs(orig_x) - abs(x) > range_x:
				eggWalker_operandFlip_X = True
		
		#Y pathing
		if (abs(y) - abs(orig_y)) < range_y and eggWalker_operandFlip_Y: #Smaller than max and direction hasn't just been reversed
			#y = y + (float(randint(0,90)) / 1000000)
			y = y + 0.000000 + (float(randint(0,25)) / 1000000)
		else:
			print("Y-reverse")
			eggWalker_operandFlip_Y = False
			#y = y - (float(randint(0,90)) / 1000000)
			y = y - 0.000000 - (float(randint(0,25)) / 1000000)
			
			#Check if need to reverse direction back to original direction
			if abs(orig_y) - abs(y) > range_y:
				eggWalker_operandFlip_Y = True
		
		y = round(y,6)
		x = round(x,6)
		commandString = "geo fix " + str(x) + " " + str(y)
		print("commandString: " + str(commandString))
		handleTelnetSending(commandString + "\n")

		
def loadLastGeo():
	global x
	global y
	localFile = open(lastGeo_filePath, "r")
	for line in localFile:
		if 'geo fix ' in line:
			lineSplit = line.split(" ")
			x = float(lineSplit[-2])
			y = float(lineSplit[-1])

def writeCurrentGeoToFile():
	localFile = open(lastGeo_filePath, "w")
	localFile.write("geo fix " + str(x) + " " + str(y))
	localFile.close()
	
def handleTelnetSending(commandIn):
	try:
		tn.write(commandIn)
	except:
		sys.exit(1)
	
	
######################################
#geo fix -77.478124 38.728273
#geo fix -77.381557 38.972007
loadLastGeo()
answer = str(raw_input("Enter geo coord string:  [" + str(x) + " " + str(y) + "] ") or str(x) + " " + str(y))
answerList = answer.split(" ")
x = float(answerList[-2])
y = float(answerList[-1])

gps_keepAlive() #gps_keepAlive()
print("Doing input loop:")

def moveUp():
	global y
	y = y + 0.000100 + (float(randint(0,90)) / 1000000)
	y = round(y,6)
	commandString = "geo fix " + str(x) + " " + str(y)
	print("commandString: " + str(commandString))
	handleTelnetSending(commandString + "\n")
	print("moveUp")

def moveDown():
	global y
	y = y - 0.000100 - (float(randint(0,90)) / 1000000)
	y = round(y,6)
	commandString = "geo fix " + str(x) + " " + str(y)
	print("commandString: " + str(commandString))
	handleTelnetSending(commandString + "\n")
	print("moveDown")

def moveLeft():
	global x
	x = x - 0.000200 - (float(randint(0,90)) / 1000000)
	x = round(x,6)
	commandString = "geo fix " + str(x) + " " + str(y)
	print("commandString: " + str(commandString))
	handleTelnetSending(commandString + "\n")
	print("moveLeft")

def moveRight():
	global x
	x = x + 0.000200 + (float(randint(0,90)) / 1000000)
	x = round(x,6)
	commandString = "geo fix " + str(x) + " " + str(y)
	print("commandString: " + str(commandString))
	handleTelnetSending(commandString + "\n")
	print("moveRight")

from msvcrt import getch
while True:
	key = ord(getch())
	print(str(key))
	if key == 27: #ESC
		tn.write("exit\n")
		#break
		writeCurrentGeoToFile()
		enableKeepAlive = False
		enableEggWalker = False
		enableKeepAliveMinimal = False
		sys.exit(0)
	elif key == 13: #Enter
		#select()
		if not enableKeepAliveMinimal:
			enableKeepAliveMinimal = True
			enableEggWalker = False
			enableKeepAlive = False #Kill keep-alive so they aren't competing
		else:
			enableKeepAliveMinimal = False
			enableKeepAlive = True
			gps_keepAlive()
		gps_keepAliveMinimal()
		
	
	elif key == 32: #Space
		if not enableEggWalker:
			enableEggWalker = True
			enableKeepAlive = False #Kill keep-alive so they aren't competing
		else:
			enableEggWalker = False
			enableKeepAlive = True
			gps_keepAlive()
		eggWalker()
	
	elif key == 224: #Special keys (arrows, f keys, ins, del, etc.)
		key = ord(getch())
		if key == 80: #Down arrow
			moveDown()
		elif key == 72: #Up arrow
			moveUp()
		if key == 75: #Down arrow
			moveLeft()
		elif key == 77: #Up arrow
			moveRight()