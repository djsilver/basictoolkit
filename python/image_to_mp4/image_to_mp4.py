import matplotlib.animation as animation
import numpy as np
from pylab import *


dpi = 100
my_dpi = 100

def ani_frame():
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_aspect('equal')
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)

    im = ax.imshow(rand(300,300),cmap='gray',interpolation='nearest')
    im.set_clim([0,1])
    fig.set_size_inches([5,5])


    tight_layout()


    def update_img(n):
        tmp = rand(300,300)
        im.set_data(tmp)
        return im

    #legend(loc=0)
    ani = animation.FuncAnimation(fig,update_img,300,interval=30)
    writer = animation.writers['ffmpeg'](fps=30)

    ani.save('demo.mp4',writer=writer,dpi=dpi)
    return ani

	
def alt_framw_write(inputImage):
	#fig = plt.figure()
	fig = plt.figure(figsize=(1920/my_dpi, 1080/my_dpi), dpi=my_dpi)
	#ax = fig.add_subplot(111)
	#ax.set_aspect('equal')
	#ax.get_xaxis().set_visible(False)
	#ax.get_yaxis().set_visible(False)

	#im = ax.imshow(inputImage,cmap='gray',interpolation='nearest')
	#im.set_clim([0,1])
	#fig.set_size_inches([10,5])


	#tight_layout()


	def update_img(n):
		#tmp = rand(300,300)
		im.set_data(inputImage)
		return im

	#legend(loc=0)
	ani = animation.FuncAnimation(fig,plt.imshow(inputImage),300,interval=30)
	writer = animation.writers['ffmpeg'](fps=30)

	ani.save('demo.mp4',writer=writer,dpi=dpi)
	return ani

##############

#import image
import os
import glob

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
#image = mpimg.imread("chelsea-the-cat.png")
#plt.imshow(image)
#plt.show()

dst_folderPath = os.path.abspath("dst")
src_folderPath = os.path.abspath("src")
#print("src_folderPath: " + str(src_folderPath))
fileList = glob.glob(os.path.join(src_folderPath,"*.jpg"))
print "fileList " + str(fileList)

image = mpimg.imread(fileList[0])

alt_framw_write(image)

#imageIn = image.open('File.jpg')
#image.show()

