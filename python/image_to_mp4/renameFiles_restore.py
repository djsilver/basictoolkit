import os
import glob

os.chdir(".")
currentPath = os.path.abspath(".")

os.chdir(currentPath)
fileRestoreMappingList = []
currentDirName = os.getcwd()
restoreMapping = currentDirName.split('\\')[-1] + "_restoreRenameMapping.txt"

with open(restoreMapping) as f:
    fileRestoreMappingList = f.readlines()

for line in fileRestoreMappingList:
	origName = line.split(":")[0].strip()
	newName = line.split(":")[1].strip()
	
	print "Renaming: " + newName + " to " + origName
	os.rename(newName, origName)