import os
import glob

os.chdir(".")
currentPath = os.path.abspath(".")

os.chdir(currentPath)
fileList = []
count = 0
currentDirName = os.getcwd()
restoreMapping = currentDirName.split('\\')[-1] + "_restoreRenameMapping.txt"


#fileList = glob.glob("REC_*.*")
fileList = glob.glob("*.jpg")
print "fileList " + str(fileList)

print "Opening file: " + restoreMapping
f = open(restoreMapping,'w')
for file in fileList:
	newFileName = '{0:03}'.format(count) + os.path.splitext(file)[1]
	print "Renaming: " + file + " to " + newFileName
	os.rename(file, newFileName)
	
	
	f.write(file + ":" + newFileName + '\n')
	count = count + 1
f.close()
