#TabAuto v1.0
### Change log ###
# 1.0 - Works

import os
import sys
import subprocess
import datetime
import fnmatch
import shutil
import time

#<<< Vars >>>#
combineJpgFolderPath = os.path.abspath("_combine_jpg")
yearDate = int(datetime.date.today().strftime("%Y"))
MonthDate = int(datetime.date.today().strftime("%m"))
dayDate = int(datetime.date.today().strftime("%d"))
hourTime = int(datetime.datetime.now().strftime("%H"))

videoSourceFolder = "_mp4_from_jpg"
videoDoneUploadingFolder = "_uploaded_done" #Subfolder of video source folder




#Create dir if it does not exist already
def mkdir(folder):
	if not os.path.isdir(folder):
		os.mkdir(folder)			

def uploadToYoutube(targetFolderPath):
	mkdir(os.path.abspath(os.path.join(targetFolderPath, videoDoneUploadingFolder)))
	os.chdir(targetFolderPath)
	
	fileList = [name for name in os.listdir(".") if os.path.isfile(os.path.abspath(os.path.join(".", name)))] #More efficient than doing a dirwalk
	
	fileList.sort() #Want the smallest (earliest) date listed first
	#fileList.reverse() #Reverse the order since it seems the newest is listed first
	
	print("fileList: " + str(fileList))
	
	#for file in fileList:
	#	print(file)
	#print("")
	
	#raw_input("pause")
	
	#Upload the files
	for fileName in fileList:
		#youtube-upload -t 2017_1_7_Day.mp4 --privacy=private --playlist=Emily_crib 2017_1_7_Day.mp4
		#raw_input("pause")
		subprocess.call("/usr/local/bin/youtube-upload -t " + fileName + " --privacy=private --playlist=Emily_crib " + fileName, shell=True)
		#subprocess.call(['youtube-upload', ' --privacy=private', ' --playlist=Emily_crib ', '-t ' + str(fileName), ' ' + str(fileName)])
		time.sleep(1)
		#raw_input("pause")
		
		shutil.move(fileName, os.path.join(videoDoneUploadingFolder, fileName))
		time.sleep(3)
		#raw_input("pause")
		




#<<< Main script start >>>#
os.chdir(".")
startingPath = os.path.abspath(".")

#Create folders if they do not exist
mkdir(videoSourceFolder)



#Call the renameMove script, use blocking call so we wait until its done
#subprocess.call(['python', 'Dahua_renameSnapshots_v1.5_moveAndSortByDay.py'])

uploadToYoutube(targetFolderPath=videoSourceFolder)