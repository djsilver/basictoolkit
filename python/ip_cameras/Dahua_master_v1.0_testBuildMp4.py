#TabAuto v1.0
### Change log ###
# 1.0 - Works

#To reset folders for another creation of mp4, use the following:
#find . -name "*restoreRenameMapping.txt" -type f
#find . -name "*restoreRenameMapping.txt" -type f -delete

import os
import sys
import subprocess
import datetime
import fnmatch
import shutil

#<<< Vars >>>#
combineJpgFolderPath = os.path.abspath("_combine_jpg")
yearDate = int(datetime.date.today().strftime("%Y"))
MonthDate = int(datetime.date.today().strftime("%m"))
dayDate = int(datetime.date.today().strftime("%d"))
hourTime = int(datetime.datetime.now().strftime("%H"))

#todaysDateFolder = datetime.date.today().strftime("%Y-%m-%d") #2016_12_2, need to exclude 2016_12_1-2_Night, 2016_12_2_Day, 2016_12_2-3_Night until it is the next day



def recursiveFileTraversal(targetFolderPath):
	print("#$#\t recursiveFileTraversal \t#$#")
	print("targetFolderPath: " + str(targetFolderPath))
	
	#$# Get recursive list of all *.jpg files
	#Uses "recursiveFileList" var
	#global recursiveFileList
	excludeDirList = []
	#if hourTime < 8:
	#	excludeDirList.append(str(yearDate) + "_" + str(MonthDate) + "_" + str(dayDate-1) + '-' + str(dayDate) + '_Night')
	#if hourTime < 18:
	#	excludeDirList.append(str(yearDate) + "_" + str(MonthDate) + "_" + str(dayDate) + '_Day')
	#excludeDirList.append(str(yearDate) + "_" + str(MonthDate) + "_" + str(dayDate) + '-' + str(dayDate+1) + '_Night')
	print("excludeDirList: " + str(excludeDirList))

	#for root, dirnames, filenames in os.walk(targetFolderPath):
	subfolders = [name for name in os.listdir(targetFolderPath) if os.path.isdir(os.path.abspath(os.path.join(targetFolderPath, name)))] #More efficient than doing a dirwalk
	
	print("subfolders: " + str(subfolders))
	
	subfolders[:] = [d for d in subfolders if d not in excludeDirList] #Modify list in-place to remove the _combine folder from dirwalk
	print("subfolders: " + str(subfolders))
	
	#Debug
	#raw_input("pause")
	
	#for filename in fnmatch.filter(filenames, '*.jpg'):
	#for dirname in fnmatch.filter(subfolders, '\d{4}'):
		#print("dirname: " + str(dirname))
	
	
	import re
	#pattern = re.compile("^([0-9]{4}_)")
	pattern = re.compile("^([0-9]{4}_[0-9]+_)")
	for folder in subfolders:
		if pattern.match(folder):
			print("folder: " + str(folder))
			
			folderAbsPath = os.path.abspath(os.path.join(targetFolderPath, folder))
			print("folderAbsPath: " + str(folderAbsPath))
			
			needToCreateMp4 = False
			fileList = os.listdir(folderAbsPath)
			#print("fileList: " + str(fileList))
			
			#Check for indicator file (artifact of renaming files for ffmpeg)
			restoreRenameMappingFileList = fnmatch.filter(fileList, '*.txt')
			if len(restoreRenameMappingFileList) < 1:
				print("No .txt files")
				needToCreateMp4 = True
			else:
				print("Found one or more .txt files")
				needToCreateMp4 = True #Need to change default to be true, then change back to false if we find our indicator
				for filename in restoreRenameMappingFileList:
					if "restoreRenameMapping" in filename:
						#Already converted folder contents
						print("Found a restoreRenameMapping .txt file")
						needToCreateMp4 = False
			
			print("needToCreateMp4: " + str(needToCreateMp4))
			#raw_input("pause")
			if needToCreateMp4:
				#Rename the .jpg files so that ffmpeg can work
				subprocess.call(['python', 'renameFiles_v1.2.py', 'path=' + folderAbsPath])
				
				#Create the mp4 file using the .jpg files
				os.chdir(folderAbsPath) #Change to the target folder temporarily so that ffmpeg can work
				subprocess.call(['ffmpeg' ' -i' ' %03d.jpg' ' -c:v' ' libx264' ' -r' ' 30' ' -pix_fmt' ' yuv420p' ' ' + folder + '.mp4'], shell=True)
				os.chdir(startingPath) #Return to starting folder
				
				#Restore the .jpg filenames
				subprocess.call(['python', 'renameFiles_restore_v1.1.py', 'path=' + folderAbsPath])
				
				shutil.move(os.path.abspath(os.path.join(folderAbsPath, folder + '.mp4')), os.path.abspath(os.path.join(startingPath, "_mp4_from_jpg")))
				
#Create dir if it does not exist already
def mkdir(folder):
	if not os.path.isdir(folder):
		os.mkdir(folder)			
		



#<<< Main script start >>>#
os.chdir(".")
startingPath = os.path.abspath(".")

#Create folders if they do not exist
mkdir("_combine_jpg")
mkdir("_mp4_from_jpg")

#Call the renameMove script, use blocking call so we wait until its done
subprocess.call(['python', 'Dahua_renameSnapshots_v1.6_moveAndSortByDay.py'])

#Create the mp4
recursiveFileTraversal(targetFolderPath=combineJpgFolderPath)

#Upload mp4 to youtube
#subprocess.call(['python', 'uploadToYoutube.py'])





#Find folders in _combine_jpg folder that have not been converted to mp4 yet. Exclude the dirs for today's date

#ffmpeg -i %03d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4

