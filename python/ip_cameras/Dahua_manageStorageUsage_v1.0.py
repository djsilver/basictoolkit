#TabAuto v1.0
### Change log ###
# 1.0 - Works


#time.strftime('%Y-%m-%d_%H-%M-%S', time.gmtime(os.path.getmtime(file))) #UTC
#time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(os.path.getctime(file))) #Local time zone


import fnmatch
import os
import time
import shutil
import sys
 

startingDir = os.getcwd()



#$# Commandline start
#cd <folder>
#python <script> <args>
#python3 driveFullPrevention.py path=/media/ubi/Red8/home/stream/www/1D00611PAU00184/_combine_dav

from datetime import datetime #Used to time the runtime of the script #For dateCutoff
startTime = datetime.now() #Used to time the runtime of the script

import time
import os
import shutil
import random
import sys
import logging
import traceback #Used to output exception details
import glob #Used in exception handling of exif tags to use previous file as a template for current file
import json #Used to read config file
import datetime as DT #For dateCutoff
#from time import mktime #For dateCutoff



#<<< Vars >>>#
currentPath = os.path.abspath(".")
filePathRaw = None
maxDriveFullPercent = None
minPathPercent = None

for arg in sys.argv:
	#logging.info("arg: " + str(arg))
	
	if "path=" in arg:
		pathRaw = arg.split("=")[1]
		fullPath = os.path.abspath(pathRaw)
		if not os.path.isdir(fullPath):
			print("Invalid folder path: " + pathRaw)
			#logging.error("Invalid file path: " + fullPath)
			sys.exit(1)
		else:
			currentPath = fullPath
	
	if "maxDriveFullPercent=" in arg or "mdfp=" in arg:
		maxDriveFullPercent = int(arg.split("=")[1].strip())
		print("maxDriveFullPercent: " + str(maxDriveFullPercent))
	
	if "minPathPercent=" in arg or "mpp=" in arg:
		minPathPercent = int(arg.split("=")[1].strip())
		print("minPathPercent: " + str(minPathPercent))



#<<< Logging >>>#
logFileName = "Dahua_manageStorageUsage"
	
#print(logFileName)
logFilePathAndName = os.path.join(currentPath,logFileName + '.log')
print("logFilePathAndName: " + str(logFilePathAndName))
#print (logFilePathAndName)
logging.basicConfig(filename=logFilePathAndName,level=logging.DEBUG)
#logging.debug('This message should go to the log file')
logging.info('\n\n')
logging.info('Starting log...')
#logging.warning('And this, too')
logging.info('uniquestring')
dateTimeStart = time.strftime('%Y-%m-%d_%H-%M-%S') #Current date/time (local time zone)
logging.info("dateTimeStart: " + str(dateTimeStart))

logging.info("arg: " + str(arg))

def restartLogging(logFileNameIn):
	logging.info("#$#\t restartLogging \t#$#")
	print("\n\n#$#\t restartLogging \t#$#")
	print("logFileNameIn: " + str(logFileNameIn))
	
	for handler in logging.root.handlers[:]:
		logging.root.removeHandler(handler)
	logging.basicConfig(filename=logFileNameIn,level=logging.DEBUG)



def loadConfig():
	logging.info("#$#\t loadConfig \t#$#")
	#import json
	global maxDriveFullPercent
	global minPathPercent
	
	with open('driveFullPrevention_config.json') as json_data:
		config_data = json.load(json_data)
		#print(config_data)
		
		#maxDriveFullPercent = config_data.get("maxDriveFullPercentage")
		#print("(loadConfig) maxDriveFullPercent: " + str(maxDriveFullPercent))
		
		#pathDeletePriorityList = config_data.get("pathDeletePriority")
		#print("(loadConfig) pathDeletePriorityList: " + str(pathDeletePriorityList))
		
		#pathDeletePriority_preferredKeepDaysList = config_data.get("pathDeletePriority_preferredKeepDays")
		#print("(loadConfig) pathDeletePriority_preferredKeepDaysList: " + str(pathDeletePriority_preferredKeepDaysList))
		
		#pathDeletePriority_requiredKeepDaysList = config_data.get("pathDeletePriority_requiredKeepDays")
		#print("(loadConfig) pathDeletePriority_requiredKeepDaysList: " + str(pathDeletePriority_requiredKeepDaysList))
		
		
		return(config_data)



def getDiskSpaceStats(pathIn):
	#logging.info("#$#\t getDiskSpaceStats \t#$#")
	statvfs = os.statvfs(pathIn)
	
	diskSize = statvfs.f_frsize * statvfs.f_blocks     # Size of filesystem in bytes
	#diskFree = statvfs.f_frsize * statvfs.f_bfree      # Actual number of free bytes
	diskFree = statvfs.f_frsize * statvfs.f_bavail #Free to non-priv users
	
	return(diskSize, diskFree)


def getCurrentFolderContents(path):
	logging.info("#$#\t getCurrentFolderContents \t#$#")
	folders = []
	files = []
	for entry in os.scandir(path):
		if entry.is_dir():
			folders.append(entry.path)
		elif entry.is_file():
			files.append(entry.path)

	#print('Folders:')
	#print(folders)
	return([folders, files])


def withinRetentionDays(objectDate, daysToRetain):
	#objectDate = "2017-02-04"
	print("daysToRetain: " + str(daysToRetain))
	
	objectDate_datetime = datetime.strptime(objectDate, "%Y-%m-%d")
	endRetentionDate = objectDate_datetime + DT.timedelta(days=daysToRetain) #daysToRetain could be 60
	
	if endRetentionDate < datetime.now():
		print("Past retention limit")
		return(False)
	else:
		return(True)


def removeOldestFolder(pathIn, maxDriveFullPercent, preferredDaysKept, requiredDaysKept, deletePreferred=False):
	logging.info("#$#\t removeOldestFolder \t#$#")
	
	folders, files = getCurrentFolderContents(path=pathIn)
	
	#folders.sort() #Will put 2017_1_10 before 2017_1_2
	#Create a correct sort of folder names
	foldersDates = []
	for folder in folders:
		currentDirName = os.path.basename(os.path.normpath(folder)) #Get name of current directory
		folderNameDate = currentDirName.split("_Day")[0].split("_Night")[0].split("-")[0].replace("_0","_1") #replace("_0","_1") fixes my lazy night time folder date naming
		objectDate_datetime = datetime.strptime(folderNameDate.replace("_","-"), "%Y-%m-%d")
		foldersDates.append(objectDate_datetime)
	foldersDates, folders = (list(t) for t in zip(*sorted(zip(foldersDates, folders))))
	
	print("folders: " + str(folders))
	#print("foldersDates: " + str(foldersDates))
	
	#Delete the oldest folder, then check if the free disk space is sufficient
	diskSize, diskFree = getDiskSpaceStats(currentPath)
	diskFullPercentage = (diskSize - diskFree) / diskSize * 100
	folderIndex = 0
	while (diskFullPercentage > maxDriveFullPercent) and (len(folders) > 0) and (folderIndex < len(folders)): #Disk is fuller than allowed, should delete some files
		print(str(folders[folderIndex]))
		#Check if the folder is within the preferredDaysKept limit. Example folder names: 2017_11_26-27_Night, 2017_11_26_Day
		currentDirName = os.path.basename(os.path.normpath(folders[folderIndex])) #Get name of current directory
		folderNameDate = currentDirName.split("_Day")[0].split("_Night")[0].split("-")[0].replace("_0","_1") #replace("_0","_1") fixes my lazy night time folder date naming
		#print("currentDirName: " + str(currentDirName))
		
		if not withinRetentionDays(folderNameDate.replace("_","-"), preferredDaysKept):
			print("Deleting oldest folder: " + str(folders[folderIndex]))
			logging.info("Deleting oldest folder: " + str(folders[folderIndex]))
			shutil.rmtree(folders[folderIndex]) #will delete a directory and all its contents.
			del folders[folderIndex]
			
			diskSize, diskFree = getDiskSpaceStats(currentPath)
			diskFullPercentage = (diskSize - diskFree) / diskSize * 100
			print("Now diskFullPercentage: " + str(diskFullPercentage))
			logging.info("Now diskFullPercentage: " + str(diskFullPercentage))
		else:
			if deletePreferred:
				if not withinRetentionDays(folderNameDate.replace("_","-"), requiredDaysKept):
					print("Deleting oldest folder: " + str(folders[folderIndex]))
					logging.info("Deleting oldest folder: " + str(folders[folderIndex]))
					shutil.rmtree(folders[folderIndex]) #will delete a directory and all its contents.
					del folders[folderIndex]
					
					diskSize, diskFree = getDiskSpaceStats(currentPath)
					diskFullPercentage = (diskSize - diskFree) / diskSize * 100
					print("Now diskFullPercentage: " + str(diskFullPercentage))
					logging.info("Now diskFullPercentage: " + str(diskFullPercentage))
				else:
					print("Within requiredDaysKept, skipping...")
					folderIndex = folderIndex + 1
			else:
				print("Within preferredDaysKept, skipping...")
				folderIndex = folderIndex + 1
		
		#input("Pause")
	
	
	
	#os.remove() will remove a file.
	#os.rmdir() will remove an empty directory.
	#shutil.rmtree() will delete a directory and all its contents.


def configModeHandler(config_data):
	logging.info("#$#\t configModeHandler \t#$#")
	#diskSize, diskFree = getDiskSpaceStats(currentPath)
	
	maxDriveFullPercent = config_data.get("maxDriveFullPercentage")
	pathDeletePriorityList = config_data.get("pathDeletePriority")
	pathDeletePriority_preferredKeepDaysList = config_data.get("pathDeletePriority_preferredKeepDays")
	pathDeletePriority_requiredKeepDaysList = config_data.get("pathDeletePriority_requiredKeepDays")
	
	#Loop through paths, deleting up to preferredDaysKept
	for index, fullPath in enumerate(pathDeletePriorityList):
		#Call function to start deleting folders in the current path until there is enough free space. If all folders are deleted and there is still not enough free space, then the below space catch will continue on to the next path
		removeOldestFolder(fullPath, maxDriveFullPercent, pathDeletePriority_preferredKeepDaysList[index], pathDeletePriority_requiredKeepDaysList[index])
		
		#Check if there is now enough free disk space
		diskSize, diskFree = getDiskSpaceStats(currentPath)
		diskFullPercentage = (diskSize - diskFree) / diskSize * 100
		print("diskFullPercentage: " + str(diskFullPercentage))
		if (diskFullPercentage < maxDriveFullPercent): #Disk is no longer fuller than allowed, will exit
			return()
		#input("Pause")
		
	#Will only reach this code if disk is still full, due to return() above
	#Looped through paths under preferredDaysKept, now loop through folders under requiredDaysKept
	for index, fullPath in enumerate(pathDeletePriorityList):
		#Call function to start deleting folders in the current path until there is enough free space. If all folders are deleted and there is still not enough free space, then the below space catch will continue on to the next path
		removeOldestFolder(fullPath, maxDriveFullPercent, pathDeletePriority_preferredKeepDaysList[index], pathDeletePriority_requiredKeepDaysList[index], deletePreferred=True)
		
		#Check if there is now enough free disk space
		diskSize, diskFree = getDiskSpaceStats(currentPath)
		diskFullPercentage = (diskSize - diskFree) / diskSize * 100
		print("diskFullPercentage: " + str(diskFullPercentage))
		if (diskFullPercentage < maxDriveFullPercent): #Disk is no longer fuller than allowed, will exit
			return()
		#input("Pause")
		
	


#<<< Main script start >>>#
config_data = None
try:
	config_data = loadConfig()
	maxDriveFullPercent = config_data.get("maxDriveFullPercentage")
	currentPath = config_data.get("pathDeletePriority")[0]
except:
	pass

diskSize, diskFree = getDiskSpaceStats(currentPath)
print("diskSize: " + str(round(diskSize/1024/1024/1024, 2)) + "GB")
print("diskFree: " + str(round(diskFree/1024/1024/1024, 2)) + "GB")

diskFullPercentage = (diskSize - diskFree) / diskSize * 100
print("diskFullPercentage: " + str(diskFullPercentage))

if (diskFullPercentage > maxDriveFullPercent): #Disk is fuller than allowed, should delete some files
	print("Disk is fuller than allowed, should delete some files")
	
	
	if config_data:
		#Config method
		configModeHandler(config_data)
		
	#removeOldestFolder(combineFolderPath)
	
	

else:
	print("Drive is NOT fuller than allowed, exiting...")

print("Runtime: " + str(datetime.now() - startTime))
logging.info("Runtime: " + str(datetime.now() - startTime))