#time.strftime('%Y-%m-%d_%H-%M-%S', time.gmtime(os.path.getmtime(file))) #UTC
#time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(os.path.getctime(file))) #Local time zone


import fnmatch
import os
import time
import shutil
import sys
 

startingDir = os.getcwd()


def recursiveFileTraversal(targetFolderPath):
	#$# Get recursive list of all *.jpg files
	#Uses "recursiveFileList" var
	#global recursiveFileList
	excludeDirList = ["_combine", "_combine_jpg", "_mp4_from_jpg"]
	for root, dirnames, filenames in os.walk(startingDir):
		dirnames[:] = [d for d in dirnames if d not in excludeDirList] #Modify list in-place to remove the _combine folder from dirwalk
		#print("dirnames: " + str(dirnames))
		#raw_input("pause")
		
		for filename in fnmatch.filter(filenames, '*.jpg'):
			#recursiveFileList.append(os.path.join(root, filename))
			#print("root: " + str(root))
			#print("filename: " + str(filename))
			
			fullFilePath = os.path.join(root, filename)
			newFileName = createNewFileName(filenameIn=filename, fullFilePathIn=fullFilePath)
			#newFullFilePath = os.path.join(targetFolderPath, newFileName)
			
			targetFolderPath_sortedByDay = sortTargetFileByDayOrNight(sourceFullFilePath=fullFilePath, targetFolderPath=targetFolderPath)
			
			#Debug
			#print("newFileName: " + str(newFileName))
			#print("targetFolderPath_sortedByDay: " + str(targetFolderPath_sortedByDay))
			
			newFullFilePath = os.path.join(targetFolderPath_sortedByDay, newFileName)
			
			#print("fullFilePath: " + str(fullFilePath))
			#print("newFullFilePath: " + str(newFullFilePath))
			
			moveFileToFolder(sourceFullFilePath=fullFilePath, targetFullFilePath=newFullFilePath)
			
	#print(recursiveFileList)


#$# Check if .jpg from Dahua (ex: "23[R][0@0][0]")
# - Get last-modified-date-time
# - Rename the files to be "YYYY-mm-dd_HH-MM-SS_<origFilename>"
# - Move into current folder

def createNewFileName(filenameIn, fullFilePathIn):
	#global oldFileToNewFilePathDict
		#print fileName
		
		#Check if already renamed
		if "_#" in filenameIn: #Already renamed
			return(filenameIn)
		else: #Note renamed yet
			if filenameIn.count("[") == 3 and filenameIn.count("]") == 3: #Should be a Dahua snapshot
				#print(fileName)
				fileDateTime = time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(os.path.getmtime(fullFilePathIn))) #Local time zone
				#print(fileDateTime)
				newFileName = fileDateTime + "_#" + filenameIn
				return(newFileName)

def moveFileToFolder(sourceFullFilePath, targetFullFilePath):
	if not os.path.exists(targetFullFilePath):
		try:
			shutil.move(sourceFullFilePath,targetFullFilePath)
			print("Moved to combine folder: " + os.path.basename(sourceFullFilePath))
		except WindowsError:
			print("Error copying: " + os.path.basename(sourceFullFilePath))
	else:
		print("File already moved")

def OLD_sortTargetFileByDay(sourceFullFilePath, targetFolderPath):
	ftime = time.localtime(os.path.getmtime(sourceFullFilePath))
	#time.localtime(os.path.getmtime(fullFilePath))) #Local time zone
	ctime = time.gmtime(os.path.getctime(sourceFullFilePath))
	#print(ftime)
	#print(ctime)
	ctime_dir = str(ftime.tm_year) + '_' + str(ftime.tm_mon)# + '_' + str(ftime.tm_mday)
	
	
	ctime_dir = ctime_dir + '_' + str(int(ftime.tm_mday))
	targetFolderPath_sortedByDay = os.path.join(targetFolderPath, ctime_dir)
		
	if not os.path.isdir(targetFolderPath_sortedByDay):
		os.mkdir(targetFolderPath_sortedByDay)
	return(targetFolderPath_sortedByDay)

def sortTargetFileByDayOrNight(sourceFullFilePath, targetFolderPath):
	ftime = time.localtime(os.path.getmtime(sourceFullFilePath))
	#time.localtime(os.path.getmtime(fullFilePath))) #Local time zone
	ctime = time.gmtime(os.path.getctime(sourceFullFilePath))
	#print(ftime)
	#print(ctime)
	
	dayDate = int(ftime.tm_mday)
	ctime_dir = str(ftime.tm_year) + '_' + str(ftime.tm_mon)# + '_' + str(dayDate)
	#ctime_dir = ctime_dir + '_' + str(int(ftime.tm_mday)) #Is this necessary? Should roll into above line, not sure why the int()
	
	#Get the hour of the image
	fileHour = int(time.strftime('%H', time.localtime(os.path.getmtime(sourceFullFilePath)))) #Local time zone
	
	#If the hour is between zero inclusive and 8 exclusive then consider morning. 8 inclusive till 18 exclusive is daytime. 18 inclusive till 24 inclusive is nighttime.
	if fileHour < 8:
		dayNightSwitch = "Night"
		ctime_dir = ctime_dir + '_' + str(dayDate-1) + '-' + str(dayDate) #Combine with previous day's Night
	elif fileHour >= 8 and fileHour < 18:
		dayNightSwitch = "Day"
		ctime_dir = ctime_dir + '_' + str(dayDate)
	else: # fileHour >= 18
		dayNightSwitch = "Night"
		ctime_dir = ctime_dir + '_' + str(dayDate) + '-' + str(dayDate+1) #Combine with next day's Night
	ctime_dir = ctime_dir + '_' + dayNightSwitch #Add the dayNightSwitch to the folder name
	
	targetFolderPath_sortedByDay = os.path.join(targetFolderPath, ctime_dir)
		
	if not os.path.isdir(targetFolderPath_sortedByDay):
		os.mkdir(targetFolderPath_sortedByDay)
	return(targetFolderPath_sortedByDay)




#<<< Main script start >>>#
#combineFolderPath = os.path.abspath(os.path.join(os.path.abspath(os.pardir), "_combine"))
combineFolderPath = os.path.abspath("_combine_jpg")
if not os.path.exists(combineFolderPath):
	os.makedirs(combineFolderPath)

recursiveFileTraversal(targetFolderPath=combineFolderPath)
