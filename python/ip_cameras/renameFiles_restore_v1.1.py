import os
import glob
import sys

pathInput = "."

#Get any commandline arguments
for arg in sys.argv:
	#print("arg: " + str(arg))
	
	if "path=" in arg:
		pathInput = arg.split("=")[1]
	

os.chdir(pathInput)
currentPath = os.path.abspath(".")

os.chdir(currentPath)
fileRestoreMappingList = []
currentDirName = os.path.basename(os.path.normpath(os.getcwd())) #Get name of current directory
restoreMappingFileName = currentDirName + "_restoreRenameMapping.txt"
restoreMappingFullFilePath = os.path.abspath(os.path.join(currentPath, restoreMappingFileName))


with open(restoreMappingFullFilePath) as f:
    fileRestoreMappingList = f.readlines()

for line in fileRestoreMappingList:
	origName = line.split(":")[0].strip()
	newName = line.split(":")[1].strip()
	
	print "Renaming: " + newName + " to " + origName
	os.rename(newName, origName)