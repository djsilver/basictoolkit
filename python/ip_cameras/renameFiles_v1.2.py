import os
import glob
import sys

pathInput = "."

#Get any commandline arguments
for arg in sys.argv:
	#print("arg: " + str(arg))
	
	if "path=" in arg:
		pathInput = arg.split("=")[1]
	

os.chdir(pathInput)
currentPath = os.path.abspath(".")
print("currentPath: " + str(currentPath))

os.chdir(currentPath)
fileList = []
count = 0
currentDirName = os.path.basename(os.path.normpath(os.getcwd())) #Get name of current directory
restoreMappingFileName = currentDirName + "_restoreRenameMapping.txt"
restoreMappingFullFilePath = os.path.abspath(os.path.join(currentPath, restoreMappingFileName))


#fileList = glob.glob("REC_*.*")
fileList = glob.glob("*.jpg")
fileList.sort() #Need to sort because the order is not guaranteed on linux
#print "fileList " + str(fileList)

print "Opening file: " + restoreMappingFullFilePath
f = open(restoreMappingFullFilePath,'w')
for file in fileList:
	newFileName = '{0:03}'.format(count) + os.path.splitext(file)[1]
	print "Renaming: " + file + " to " + newFileName
	os.rename(file, newFileName)
	
	
	f.write(file + ":" + newFileName + '\n')
	count = count + 1
f.close()
