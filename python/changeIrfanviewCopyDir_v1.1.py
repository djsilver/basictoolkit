#changeIrfanviewCopyDir_v1.1.py
### Change log ###
# 1.0 - Works
# 1.1 - Handles the ini file being in several locations


import os
import shutil
import glob
import time

os.chdir(".")
currentPath = os.path.abspath(".")

irfanViewIniFileList = []
irfanViewIniFileList.append(os.path.abspath(os.getenv('APPDATA') + "\\IrfanView\\i_view32.ini"))
irfanViewIniFileList.append(os.path.abspath(os.getenv('APPDATA') + "\\..\\Local\VirtualStore\Program Files\IrfanView_433\\i_view32.ini"))
irfanViewIniFileList.append(os.path.abspath(os.getenv('APPDATA') + "\\..\\Local\VirtualStore\Program Files\IrfanView\\i_view32.ini"))
irfanViewIniFileList.append(os.path.abspath(os.getenv('APPDATA') + "\\..\\Local\VirtualStore\Windows\\i_view32.ini"))


def backupSettingsFile(inFilePath):
	#Backup settings
	dateTime = time.strftime('%Y-%m-%d_%H-%M-%S') #Current date/time (local time zone)
	shutil.copy(inFilePath, inFilePath + "_" + dateTime + ".bak")


def changeCopyDirSettings(inFilePath):
	fileContentsList = []

	with open(inFilePath) as f:
		for row in f:
			row = row.strip()
			if "AskCopy=" in row:
				row = "AskCopy=0"
			if "CopyDir=" in row:
				row = "CopyDir=" + currentPath
				print("Changed copyDir to: " + currentPath)
			
			fileContentsList.append(row)
	f.close()
			
	print(fileContentsList)

	outputFileContents = '\n'.join(fileContentsList) #Creates string with '\n' as the separator character

	f = open(inFilePath,'w')
	f.write(outputFileContents)
	f.close()



#<<< Main script start >>>#
for irfanViewIniFile in irfanViewIniFileList:
	print(irfanViewIniFile)
	#See if the file exists
	if os.path.isfile(irfanViewIniFile):
		print("File exists")
		backupSettingsFile(irfanViewIniFile)
		changeCopyDirSettings(irfanViewIniFile)




#TODO

#Add lines if they don't exist in the ini file

#[Copy-Move]
#AskCopy=0
#AskMove=1
#CopyDir=D:\TempINI
#MoveDir=D:\TempINI