#!/usr/bin/python
#youtube-dl_auto v1.0
### Change log ###
# 1.0 - Works


#Deps
#sudo apt-get install <package>
#sudo pip install <package>
#Update: sudo pip install -U youtube-dl

#$# Commandline start
#cd <folder>
#python <script> <args>
#cd /media/ubi/Red7/work/youtube-dl/
#python youtube-dl_auto.py


#youtube-dl --ignore-errors -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o '%(title)s.%(ext)s' 


import logging
import os
import subprocess
import random
import time

from datetime import datetime #Used to time the runtime of the script #For dateCutoff
startTime = datetime.now() #Used to time the runtime of the script

#<<< Vars >>>#
currentPath = os.path.abspath(".")
oneTimeGetListFile = "_getlist.txt"
repeatGetListFile = "_getlist_repeat.txt"


#<<< Logging >>>#
logFileName = "youtube-dl_auto"

#print(logFileName)
logFilePathAndName = os.path.join(currentPath,logFileName + '.log')
print("logFilePathAndName: " + str(logFilePathAndName))
#print (logFilePathAndName)
logging.basicConfig(filename=logFilePathAndName,level=logging.DEBUG)
#logging.debug('This message should go to the log file')
logging.info('\n\n')
logging.info('Starting log...')
#logging.warning('And this, too')
logging.info('uniquestring')


def restartLogging(logFileNameIn):
	logging.info("#$#\t restartLogging \t#$#")
	print("\n\n#$#\t restartLogging \t#$#")
	print("logFileNameIn: " + str(logFileNameIn))
	
	for handler in logging.root.handlers[:]:
		logging.root.removeHandler(handler)
	logging.basicConfig(filename=logFileNameIn,level=logging.DEBUG)

def sleepShort(min=2, max=4, printSleepTime=False):
	#time.sleep(random.uniform(0.5,2))
	sleepTime = random.uniform(min,max)
	if printSleepTime:
		print("sleepTime: " + str(sleepTime))
	time.sleep(sleepTime)

def sleepLong(min=1200, max=2400, printSleepTime=False):
	#tempRand = random.uniform(1200,2400)
	tempRand = random.uniform(min,max)
	logging.error("\n ERROR:429 server is blocking requests. Sleeping " + str(tempRand/60) + " minutes\n")
	print("\n ERROR:429 server is blocking requests. Sleeping " + str(tempRand/60) + " minutes\n")
	print("Will retry same page after sleep")
	try:
		time.sleep(tempRand) #Some random time between 30 and 60 minutes
	except:
		print("Stopping sleep and continuing...")

def callYoutubeDL(urlIn, playlist=False):
	#youtube-dl --ignore-errors -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o '%(title)s.%(ext)s' 
	#youtube-dl --ignore-errors -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o '%(playlist)s/%(title)s.%(ext)s'
	
	if "playlist?list=" in urlIn:
		playlist = True
	
	#Video download, use -k to keep the separate video and audio file parts
	if playlist:
		callString = "youtube-dl --ignore-errors -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o '%(playlist)s/%(title)s.%(ext)s' " + urlIn
	else:
		#callString = ['youtube-dl', '--ignore-errors', '-f', "'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best'", '-o', "'%(title)s.%(ext)s'", urlIn]
		callString = "youtube-dl --ignore-errors -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best' -o '%(title)s.%(ext)s' " + urlIn
	
	#Python call bash
	print("callString: " + str(callString))
	subprocess.call(callString, shell=True)
	
	
	
	#Music download
	if playlist:
		callString = "youtube-dl --ignore-errors -f 'bestaudio' -x --audio-format mp3 -o '%(playlist)s_mp3/%(title)s.%(ext)s' " + urlIn
	else:
		#callString = ['youtube-dl', '--ignore-errors', '-f', "'bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best'", '-o', "'%(title)s.%(ext)s'", urlIn]
		callString = "youtube-dl --ignore-errors -f 'bestaudio' -x --audio-format mp3 -o '_mp3/%(title)s.%(ext)s' " + urlIn
	
	#Python call bash
	print("callString: " + str(callString))
	subprocess.call(callString, shell=True)


def getFileContents(pathIn):
	fullFilePathIn = os.path.abspath(pathIn)
	
	with open(fullFilePathIn) as file:
		fileLines = file.readlines()

	return(fileLines)


def handleFileList(pathIn):
	urlList = getFileContents(pathIn)
	
	for url in urlList:
		url = url.strip()
		callYoutubeDL(url)
		sleepShort()






#<<< Main script start >>>#

#One time get URLs (generally not playlists, but can be)
handleFileList(oneTimeGetListFile)

#Urls that you want to keep getting (such as playlists)
handleFileList(repeatGetListFile)





print("Runtime: " + str(datetime.now() - startTime))
logging.info("Runtime: " + str(datetime.now() - startTime))







