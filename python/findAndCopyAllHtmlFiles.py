#time.strftime('%Y-%m-%d_%H-%M-%S', time.gmtime(os.path.getmtime(file))) #UTC
#time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(os.path.getctime(file))) #Local time zone


import fnmatch
import os
import time
import shutil

startingDir = os.getcwd()
recursiveFileList = []
oldFileToNewFilePathDict = {}


def getRecursiveFileList():
	#$# Get recursive list of all *.html files
	#Uses "recursiveFileList" var
	#global recursiveFileList
	for root, dirnames, filenames in os.walk(startingDir):
		for filename in fnmatch.filter(filenames, '*.html'):
			recursiveFileList.append(os.path.join(root, filename))
	#print(recursiveFileList)


#$# Check if .jpg from Dahua (ex: "23[R][0@0][0]")
# - Get last-modified-date-time
# - Rename the files to be "YYYY-mm-dd_HH-MM-SS_<origFilename>"
# - Move into current folder

def createNewFileNamePairings():
	#global oldFileToNewFilePathDict
	for fullFilePath in recursiveFileList:
		fileName = os.path.basename(fullFilePath)
		folderPath = os.path.dirname(fullFilePath)
		newFullFilePath = ""
		#print fileName
		
		#Check if already renamed
		if "_#" in fileName: #Already renamed
			continue
		else: #Note renamed yet
			if fileName.count("[") == 3 and fileName.count("]") == 3: #Should be a Dahua snapshot
				#print(fileName)
				fileDateTime = time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime(os.path.getmtime(fullFilePath))) #Local time zone
				#print(fileDateTime)
				newFileName = fileDateTime + "_#" + fileName
				newFullFilePath = os.path.join(folderPath, newFileName)
				
				oldFileToNewFilePathDict[fullFilePath] = newFullFilePath #Store the pairing of the old fullFilePath and the newFullFilePath

def renameFilesUsingPairingDict():
	for key, value in oldFileToNewFilePathDict.iteritems():
		shutil.move(key,value)
		print("Renamed to: " + os.path.basename(value))


def copyFilesToFolder(fileFilterString, targetFolderPath):
	for fullFilePath in recursiveFileList:
		if fileFilterString in fullFilePath and targetFolderPath not in fullFilePath:
			fileName = os.path.basename(fullFilePath)
			targetFullFilePath = os.path.join(targetFolderPath,fileName)
			if not os.path.exists(targetFullFilePath):
				try:
					shutil.copy(fullFilePath,targetFullFilePath)
					print("Copied to combine folder: " + os.path.basename(fullFilePath))
				except WindowsError:
					print("Error copying: " + os.path.basename(fullFilePath))
			else:
				print("File already copied")







#<<< Main script start >>>#
#getRecursiveFileList()
#createNewFileNamePairings()
#print(oldFileToNewFilePathDict)
#renameFilesUsingPairingDict()

#$#Copy the files to a "combine" folder
getRecursiveFileList()

if not os.path.exists("combine"):
	os.makedirs("combine")
combineFolderPath = os.path.abspath("combine")
#print(combineFolderPath)
fileFilterString = ".html"
copyFilesToFolder(fileFilterString, combineFolderPath)