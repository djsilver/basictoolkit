#!/usr/bin/python
#driveFullPrevention v1.0
### Change log ###
# 1.0 - Works


#Deps
#sudo apt-get install <package>
#sudo pip install <package>

#$# Commandline start
#cd <folder>
#python <script> <args>


import time
import os
import shutil
import random
import sys
import logging
import traceback #Used to output exception details
import glob #Used in exception handling of exif tags to use previous file as a template for current file


#<<< Vars >>>#
currentPath = os.path.abspath(".")


for arg in sys.argv:
	#logging.info("arg: " + str(arg))
	
	if "file=" in arg or "f=" in arg:
		filePathRaw = arg.split("=")[1]
		fullFilePath = os.path.abspath(filePathRaw)
		if os.path.isfile(fullFilePath):
			sourceFileIdList = True
			sourceFileIdList_fullFilePath = fullFilePath
			saveFolderMainGallery = os.path.basename(sourceFileIdList_fullFilePath).split(".")[0]
		else:
			print("Invalid file path (raw): " + filePathRaw)
			print("Invalid file path (abspath): " + fullFilePath)
			#logging.error("Invalid file path (raw): " + filePathRaw)
			#logging.error("Invalid file path (abspath): " + fullFilePath)
	
	if "tag=" in arg:
		galleryTag = arg.split("=")[1].strip()
		print("galleryTag: " + str(galleryTag))
	
	if "page=" in arg:
		galleryTag_resumePage = int(arg.split("=")[1].strip())
		print("galleryTag_resumePage: " + str(galleryTag_resumePage))



#<<< Logging >>>#
if sourceFileIdList_fullFilePath:
	logFileName = saveFolderMainGallery
if galleryHtmlFile:
	logFileName = os.path.basename(galleryHtmlFile).split(".")[0] + "_images"
if galleryHtmlFolder:
	logFileName = "galleryHtmlFolder"
	
#print(logFileName)
logFilePathAndName = os.path.join(currentPath,logFileName + '.log')
print("logFilePathAndName: " + str(logFilePathAndName))
#print (logFilePathAndName)
logging.basicConfig(filename=logFilePathAndName,level=logging.DEBUG)
#logging.debug('This message should go to the log file')
logging.info('\n\n')
logging.info('Starting log...')
#logging.warning('And this, too')
logging.info('uniquestring')

logging.info("arg: " + str(arg))

def restartLogging(logFileNameIn):
	logging.info("#$#\t restartLogging \t#$#")
	print("\n\n#$#\t restartLogging \t#$#")
	print("logFileNameIn: " + str(logFileNameIn))
	
	for handler in logging.root.handlers[:]:
		logging.root.removeHandler(handler)
	logging.basicConfig(filename=logFileNameIn,level=logging.DEBUG)

def sleepShort(min=2, max=4, printSleepTime=False):
	#time.sleep(random.uniform(0.5,2))
	sleepTime = random.uniform(min,max)
	if printSleepTime:
		print("sleepTime: " + str(sleepTime))
	time.sleep(sleepTime)

def sleepLong(min=1200, max=2400, printSleepTime=False):
	#tempRand = random.uniform(1200,2400)
	tempRand = random.uniform(min,max)
	logging.error("\n ERROR:429 server is blocking requests. Sleeping " + str(tempRand/60) + " minutes\n")
	print("\n ERROR:429 server is blocking requests. Sleeping " + str(tempRand/60) + " minutes\n")
	print("Will retry same page after sleep")
	try:
		time.sleep(tempRand) #Some random time between 30 and 60 minutes
	except:
		print("Stopping sleep and continuing...")

def mkdir(folder):
	if not os.path.isdir(folder):
		os.mkdir(folder)

def readFile(filePathIn):
	if os.path.isfile(filePathIn):
		with open(filePathIn, 'r') as file:
			data=file.read()
		return(data)















#<<< Main script start >>>#







