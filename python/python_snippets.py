#$#Create dir if it does not exist already
def mkdir(folder):
	if not os.path.isdir(folder):
		os.mkdir(folder)


#$#Open a webpage url in the default browser
import webbrowser
webbrowser.open('http://eample.com')


def sleepShort(min=2, max=4, printSleepTime=False):
	#time.sleep(random.uniform(0.5,2))
	sleepTime = random.uniform(min,max)
	if printSleepTime:
		print("sleepTime: " + str(sleepTime))
	time.sleep(sleepTime)


def sleepLong(min=1200, max=2400, printSleepTime=False):
	#tempRand = random.uniform(1200,2400)
	tempRand = random.uniform(min,max)
	logging.error("\n ERROR:429 server is blocking requests. Sleeping " + str(tempRand/60) + " minutes\n")
	print("\n ERROR:429 server is blocking requests. Sleeping " + str(tempRand/60) + " minutes\n")
	print("Will retry same page after sleep")
	try:
		time.sleep(tempRand) #Some random time between 30 and 60 minutes
	except:
		print("Stopping sleep and continuing...")



#$#Write out to a file
def writeOutToFile(textIn, filePathIn):
	file = open(filePathIn, 'a')
	file.write(textIn + "\n")
	file.close()


#$#Read a file
def readFile(filePathIn):
	if os.path.isfile(filePathIn):
		with open(filePathIn, 'r') as file:
			data=file.read()
		return(data)



os.path.basename(os.path.normpath(os.getcwd())) #Get name of current directory


#$#Python call bash
subprocess.call(['python', 'renameFiles_v1.2.py', 'path=' + folderAbsPath])

#$#Read file lines into list
def getFileContents(pathIn):
	fullFilePathIn = os.path.abspath(pathIn)
	
	with open(fullFilePathIn) as file:
		fileLines = file.readlines()

	return(fileLines)


#$#Python multithreading
from multiprocessing.dummy import Pool as ThreadPool
from functools import partial

multiThreadCount = 0

def loadWebpageUrl_multiThread(saveFolderIn, runMode, sourceFullFilePathIn, doneFullFilePath, failedFullFilePath, pageURL):
	logging.info("#$#\t loadWebpageUrl_multiThread \t#$#")


funcPartial = partial(loadWebpageUrl_multiThread, saveFolderIn, runMode, sourceFullFilePathIn, doneFullFilePath, failedFullFilePath) #Used to define the static vars, and leave the last var as an iterable list for the pool.map


pool = ThreadPool(multiThreadCount)
results = pool.map_async(funcPartial, sourceUrlList).get(9999999)





