#windowsFirewall_blockAll_inFolder v1.0
### Change log ###
# 1.0 - Works

### Description ###
#Create windows firewall rules to block all inbound or outbound traffic for all .exe files in the given path
#	Expects folder path as an argument, otherwise, will ask user for a folder path
#	If the user provides a file path, will only use that one file




import sys
import os
import subprocess #To call powershell
import logging #Used for debugging



#<<< Logging >>>#
logFilePathAndName = 'windowsFirewall_blockAll_inFolder.log'
#print (logFilePathAndName)
logging.basicConfig(filename=logFilePathAndName,level=logging.DEBUG)
logging.info('\n\n')
logging.info('Starting log...')


#<<< Variables >>>#
fileOutPath = "commands.txt"
try:
	givenPath = sys.argv[1] #Reuse python snippit
except:
	givenPath = None



#<<< Functions >>>#
def checkIfValidFolderOrFilePath(pathIn): #Reuse python snippit function
	if os.path.isfile(pathIn):
		return "File"
	elif os.path.isdir(pathIn):
		return "Folder"
	else:
		return False

def checkIfValidYesNoAnswerAndFormatReturn(stringIn): #Reuse python snippit function
	if stringIn.lower() == "y" or stringIn.lower() == "yes":
		return "yes"
	if stringIn.lower() == "n" or stringIn.lower() == "no":
		return "no"
	else:
		return False


def askUser(questionStringIn, loopIfEmpty=False, checkIfValidFunctionName=None, loopIfNotValid=None, defaultAnswer=None): #Reuse python snippit function
	answer = str(raw_input(questionStringIn))
	questionStringIn = questionStringIn.replace("Not valid input. ","") #Remove automatic text from looping function
	questionStringIn = questionStringIn.replace("Empty input. ","") #Remove automatic text from looping function
	
	if len(answer) > 0: #Non-empty input
		if checkIfValidFunctionName is None: #If no function validity name was passed
			#Just return the answer
			return answer
		else:
			#Put the desired function below to check the input:
			if checkIfValidFunctionName(answer):
				#Valid input
				return answer
			else:
				#Not valid input
				if loopIfNotValid:
					answer = askUser(questionStringIn = "Not valid input. " + questionStringIn, loopIfEmpty=loopIfEmpty, checkIfValidFunctionName=checkIfValidFunctionName, loopIfNotValid=loopIfEmpty, defaultAnswer=defaultAnswer)
					return answer
				else:
					return None
	else: #Empty input
		if defaultAnswer:
			return defaultAnswer
		
		if loopIfEmpty:
			answer = askUser(questionStringIn = "Empty input. " + questionStringIn, loopIfEmpty=loopIfEmpty, checkIfValidFunctionName=checkIfValidFunctionName, loopIfNotValid=loopIfEmpty, defaultAnswer=defaultAnswer)
			return answer
		else:
			return None

def getOrActOnFolderContents(pathIn, fileExt=None, functionNameIn=None): #Reuse python snippit function
	print(pathIn)
	fileList = []
	if os.path.isdir(pathIn):
		for file in os.listdir(pathIn):
			file = os.path.abspath(os.path.join(pathIn, file))
			#print(file)
			if fileExt: #If defined
				if file.endswith(fileExt):
					if functionNameIn is None: #If no function name was passed
						fileList.append(file)
					else:
						print("functionNameIn(file): " + str(file))
						#Use function to act upon file path
						functionNameIn(file)
			else:
				if functionNameIn is None: #If no function name was passed
					fileList.append(file)
				else:
					#Use function to act upon file path
					functionNameIn(file)
		return fileList
	else:
		return None

def getOrActOnFolderContentsRecursively(pathIn, fileExt=None, functionNameIn=None): #Reuse python snippit function
	fileList = []
	if os.path.isdir(pathIn):
		for root, dirs, files in os.walk(pathIn):
			for file in files:
				if fileExt: #If defined
					if file.endswith(fileExt):
						if functionNameIn is None: #If no function name was passed
							fileList.append(os.path.join(root, file))
						else:
							#Use function to act upon file path
							functionNameIn(os.path.join(root, file))
				else:
					if functionNameIn is None: #If no function name was passed
						fileList.append(os.path.join(root, file))
					else:
						#Use function to act upon file path
						functionNameIn(os.path.join(root, file))
	else:
		return None

def createWindowsFirewallBlockRules(fullFilePathIn):
	powershellGenericRuleString = 'powershell New-NetFirewallRule -DisplayName "\'<ruleName>\'" -Direction <trafficDirection> -Action <action> -EdgeTraversalPolicy Block -Protocol <protocol> -Program "\'<filePath>\'"'
	
	if os.path.exists(fullFilePathIn):
		fileName = os.path.basename(fullFilePathIn)
		parentFolder = os.path.basename(os.path.dirname(fullFilePathIn))
		ruleName = "BlockInbound-TCP_" + parentFolder + "_" + fileName
		tempRuleString = powershellGenericRuleString
		tempRuleString = tempRuleString.replace("<action>","Block")
		tempRuleString = tempRuleString.replace("<ruleName>",ruleName)
		tempRuleString = tempRuleString.replace("<trafficDirection>","Inbound")
		tempRuleString = tempRuleString.replace("<protocol>","TCP")
		tempRuleString = tempRuleString.replace("<filePath>",fullFilePathIn)
		
		print(tempRuleString)
		#subprocess.check_output(tempRuleString, shell=True)
		if not checkForExistingWinFirewallRule(ruleNameIn=ruleName):
			createWinFirewallRule(commandIn=tempRuleString, ruleNameIn=ruleName)
		
		ruleName = "BlockInbound-UDP_" + parentFolder + "_" + fileName
		tempRuleString = powershellGenericRuleString
		tempRuleString = tempRuleString.replace("<action>","Block")
		tempRuleString = tempRuleString.replace("<ruleName>",ruleName)
		tempRuleString = tempRuleString.replace("<trafficDirection>","Inbound")
		tempRuleString = tempRuleString.replace("<protocol>","UDP")
		tempRuleString = tempRuleString.replace("<filePath>",fullFilePathIn)
		
		#print("\n\n" + tempRuleString)
		if not checkForExistingWinFirewallRule(ruleNameIn=ruleName):
			createWinFirewallRule(commandIn=tempRuleString, ruleNameIn=ruleName)
		
		ruleName = "BlockOutbound-TCP_" + fileName
		tempRuleString = powershellGenericRuleString
		tempRuleString = tempRuleString.replace("<action>","Block")
		tempRuleString = tempRuleString.replace("<ruleName>",ruleName)
		tempRuleString = tempRuleString.replace("<trafficDirection>","Outbound")
		tempRuleString = tempRuleString.replace("<protocol>","TCP")
		tempRuleString = tempRuleString.replace("<filePath>",fullFilePathIn)
		
		#print(tempRuleString)
		if not checkForExistingWinFirewallRule(ruleNameIn=ruleName):
			createWinFirewallRule(commandIn=tempRuleString, ruleNameIn=ruleName)
		
		ruleName = "BlockOutbound-UDP_" + fileName
		tempRuleString = powershellGenericRuleString
		tempRuleString = tempRuleString.replace("<action>","Block")
		tempRuleString = tempRuleString.replace("<ruleName>",ruleName)
		tempRuleString = tempRuleString.replace("<trafficDirection>","Outbound")
		tempRuleString = tempRuleString.replace("<protocol>","UDP")
		tempRuleString = tempRuleString.replace("<filePath>",fullFilePathIn)
		
		#print(tempRuleString)
		if not checkForExistingWinFirewallRule(ruleNameIn=ruleName):
			createWinFirewallRule(commandIn=tempRuleString, ruleNameIn=ruleName)
			
def createWinFirewallRule(commandIn, ruleNameIn): #Powershell
	#print("\n\n" + "createWinFirewallRule")
	logging.info("commandIn: " + commandIn)
	
	#Currently no good way to run the commands as administrator, which windows firewall requires
	#Just write the command out to a file
	outFile = open(fileOutPath, "a")
	outFile.write(commandIn)
	outFile.write("\r\n\r\n")
	outFile.close()
	
	return()
	
	returnString = ""
	try:
		returnString = subprocess.check_output(commandIn, shell=True)
		if "The rule was parsed successfully from the store." in returnString:
			#Success
			print("Rule created for: " + ruleNameIn)
	except:
		#Fail
		print("ERROR creating rule for: " + ruleNameIn)
		print("returnString: " + str(subprocess.check_output(commandIn, shell=True)))

def checkForExistingWinFirewallRule(ruleNameIn):
	#print("\n\n" + "checkForExistingWinFirewallRule")
	try:
		commandString = 'powershell \'Get-NetFirewallRule -DisplayName "' + str(ruleNameIn) + '"\''
		returnString = subprocess.check_output(commandString, shell=True)
		if "The rule was parsed successfully from the store." in returnString:
			#Success
			print("Rule already exists for: " + ruleNameIn)
			return True
	except:
		#Fail
		return False
	#Get-NetFirewallRule -DisplayName "Block program"
	

#<<< Main script start >>>#
#Blank output file
outFile = open(fileOutPath, "w")
outFile.write("")
outFile.close()

if not givenPath:
	#Ask for a path
	givenPath = askUser(questionStringIn = "Please enter a folder or file path: ", loopIfEmpty=True, checkIfValidFunctionName=checkIfValidFolderOrFilePath, loopIfNotValid=True)

recursiveAnswer = askUser(questionStringIn = "Do you want to behave recursively? [n]: ", defaultAnswer="n", checkIfValidFunctionName=checkIfValidYesNoAnswerAndFormatReturn, loopIfNotValid=True)

if checkIfValidYesNoAnswerAndFormatReturn(recursiveAnswer) == "yes":
	#pass
	getOrActOnFolderContentsRecursively(givenPath, fileExt=".exe", functionNameIn=createWindowsFirewallBlockRules)
else:
	getOrActOnFolderContents(givenPath, fileExt=".exe", functionNameIn=createWindowsFirewallBlockRules)
	#print(getOrActOnFolderContents(givenPath))




